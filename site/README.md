# Sage Code Challenge Frontend

This is the frontend of the mock provided. The frontend is simple and structured with the simplest tools available.

- Pure HTML, CSS and JS
- Dragula for easy Drag and Drop api
- Parcel for bundling and HMR

## Getting Started

Install Dependencies
```bash
npm i
```
Start Local Server
```bash	
npm start
```
Build for production
```bash
npm run build
```
