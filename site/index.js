import dragula from 'dragula';

document.onreadystatechange = function() {
    if (document.readyState === 'complete') {
        const cards = document.querySelectorAll('.card');
        const drake = dragula([...cards], {
            moves: function(el, container, handle) {
                return handle.tagName === 'IMG';
            },
        });
        drake.on('drop', (el, target, source) => {
        	const destinationImg = target.querySelector('img');
        	source.prepend(destinationImg);
        });
    }
}